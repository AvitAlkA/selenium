import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 * RandomPage
 */
public class RandomPage {

    private final static String PAGE_URL = "https://www.calculator.net/random-number-generator.html";
    private WebDriver driver;

    // constructor
    public RandomPage(WebDriver d) {
        this.driver = d;
    }

    public void openPage() {
        driver.navigate().to(PAGE_URL);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // report.Info("PercentagePage", "Open " + PAGE_URL);
    }

    public WebElement LowerLimit() {
        return driver.findElement(By.cssSelector("[name=clower]"));
    }

    public WebElement UpperLimit() {
        return driver.findElement(By.cssSelector("[name=cupper]"));
    }

    public WebElement GenerateNumbersField() {
        return driver.findElement(By.cssSelector("[name=cnums]"));
    }

    public WebElement Integer() {
        return driver.findElement(By.id("cnumt1"));
    }

    public WebElement Double() {
        return driver.findElement(By.id("cnumt2"));
    }

    // value="Generate"
    public WebElement GenerateButton() {
        return driver.findElement(By.cssSelector("form[name=calform2] input[type=submit]"));
    }

    // class="h2result"

    public WebElement getResultOutput() { // GET TEXT
        return driver.findElement(By.xpath(".h2result + div > div"));
    }

    public List<WebElement> getResultOutputList() { // LIST
        return driver.findElements(By.className("verybigtext"));
    }

    public void clearFiled(WebElement element) {
        Actions act = new Actions(driver);
        act.doubleClick(element).build().perform();
    }

}
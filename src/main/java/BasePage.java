import org.openqa.selenium.WebDriver;

//import SDK.reporter;

public abstract class BasePage {
    WebDriver driver;
    // reporter report;
    String PAGE_URL;

    // constructor
    public BasePage(WebDriver d, String url) {
        this.driver = d;
        // this.report = report;
        this.PAGE_URL = url;
    }

    public void openPage() {
        driver.navigate().to(this.PAGE_URL);
        // driver.manage().window().maximize();
        // report.Info("PercentagePage", "Open " + PAGE_URL);
    }
}
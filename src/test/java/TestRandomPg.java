import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * TestRandomPg
 */
public class TestRandomPg {
    WebDriver driver = new FirefoxDriver();
    RandomPage randomPage = new RandomPage(driver);

    @BeforeTest
    public void beforeTest() {
        // open the percentage page
        randomPage.openPage();
    }

    @Test(dataProvider = "oneRandomNumber")
    public void TestRandom(String lowLimit, String upperLimit, String kind) throws InterruptedException {
        randomPage.LowerLimit().clear();
        randomPage.UpperLimit().clear();
        randomPage.GenerateNumbersField().clear();
        randomPage.LowerLimit().sendKeys(lowLimit);
        randomPage.UpperLimit().sendKeys(upperLimit);
        randomPage.GenerateNumbersField().sendKeys("1");
        Thread.sleep(1000);
        if (kind.equals("int")) {
            randomPage.Integer().click();

        } else {
            randomPage.Double().click();
        }

    }

    @DataProvider
    public Object[][] oneRandomNumber() {
        return new Object[][] { { "1", "50", "int" }, { "1.5", "50", "double" } };
    }

    @AfterMethod
    public void afterMethod() {
        // randomPage.openPage();
    }

    @AfterSuite
    public void afterSuite() {
        // driver.quit();
    }

}